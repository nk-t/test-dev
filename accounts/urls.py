from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from accounts import views

urlpatterns = [
    path('profile/', views.profile, name='profile'),

    path('profile/edit/', views.edit_profile_link, name='edit-profile-link'),

    path('profile/edit-avatar/', views.avatar, name='edit-avatar'),

    path('profile/edit-sml/', views.socialml, name='edit-socialml')
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
