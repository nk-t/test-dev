from django.db import models

from django.contrib.auth.models import User

from django.dispatch import receiver
from django.db.models.signals import post_save


class Avatar(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(
        upload_to='profile_avatars', default='default.png')

    def __str__(self):
        return f'{self.user.username} profile'


#  profile auto save signals
@receiver(post_save, sender=User)
def create_avatar(sender, instance, created, **kwargs):
    if created:
        Avatar.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_avatar(sender, instance, **kwargs):
    instance.avatar.save()


class Socialml(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    gitlab_link = models.CharField(
        max_length=400, blank=True, null=True, default='gitlab.com/name')
    bitbucket_link = models.CharField(
        max_length=400, blank=True, null=True, default='bitbucket.com/name')
    github_link = models.CharField(
        max_length=400, blank=True, null=True, default='github.com/name')
    linkedin_link = models.CharField(
        max_length=400, blank=True, null=True, default='linkedin.com/name')

    own_site = models.CharField(
        max_length=400, blank=True, null=True, default='yourname.com')

    def __str__(self):
        return f'{self.user} social media link'


@receiver(post_save, sender=User)
def create_sml(sender, instance, created, **kwargs):
    if created:
        Socialml.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_sml(sender, instance, **kwargs):
    instance.socialml.save()
