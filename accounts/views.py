from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from .forms import ChangeAvatarForm, SocialForm


@login_required
def profile(request):
    return render(request, 'accounts/profile/profile.html')


@login_required
def edit_profile_link(request):
    return render(request, 'accounts/editprofile/edit_profile_link.html')


@login_required
def avatar(request):
    if request.method == 'POST':
        ca_form = ChangeAvatarForm(
            request.POST, request.FILES, instance=request.user.avatar)
        if ca_form.is_valid():
            ca_form.save()
            return redirect('profile')
    else:
        ca_form = ChangeAvatarForm(instance=request.user.avatar)
    context = {
        'ca_form': ca_form
    }
    return render(request, 'accounts/editprofile/edit_avatar.html', context)


@login_required
def socialml(request):
    if request.method == 'POST':
        sml_form = SocialForm(request.POST, instance=request.user.socialml)
        if sml_form.is_valid():
            sml_form.save()
            return redirect('profile')
    else:
        sml_form = SocialForm(instance=request.user.socialml)
    context = {
        'sml_form': sml_form
    }
    return render(request, 'accounts/editprofile/edit_socialml.html', context)
