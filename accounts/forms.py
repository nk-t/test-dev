from django import forms

from . models import Avatar, Socialml


class ChangeAvatarForm(forms.ModelForm):
    class Meta:
        model = Avatar
        fields = ['avatar']


class SocialForm(forms.ModelForm):
    class Meta:
        model = Socialml
        fields = [
            'gitlab_link',
            'bitbucket_link',
            'github_link',
            'linkedin_link',
            'own_site'
        ]
