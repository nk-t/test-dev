from django.contrib import admin

from .models import Avatar, Socialml

admin.site.register(Avatar)
admin.site.register(Socialml)
