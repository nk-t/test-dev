from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class UserRegisterForm(UserCreationForm):

    username = forms.CharField(max_length=20, widget=forms.TextInput(
        attrs={'class': 'un', 'placeholder': '@username'}))

    email = forms.EmailField(widget=forms.EmailInput(
        attrs={'class': 'email', 'placeholder': 'xxx@email.com'}))

    password1 = forms.CharField(label='Password', widget=forms.PasswordInput(
        attrs={'class': 'up', 'placeholder': 'Enter valid password'}))

    password2 = forms.CharField(label='Confirm Password',
                                widget=forms.PasswordInput(
                                    attrs={'class': 'up', 'placeholder':
                                           'Enter same password'}))

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class UserLoginForm(forms.Form):

    username = forms.CharField(max_length=20, widget=forms.TextInput(
        attrs={'class': 'un', 'placeholder': '@username'}))

    password1 = forms.CharField(label='Password', widget=forms.PasswordInput(
        attrs={'class': 'up', 'placeholder': 'Enter valid password'}))

    # def clean_username(self):
    #     username = self.cleaned_data.get('username')
    #     if username and User.objects.filter(username=username).exists():
    #         raise forms.ValidationError(
    #             'Username already exist'
    #         )
    #     return username

    # def clean_email(self, request, format=None):
    #     email = self.cleaned_data.get('email')
    #     if email and User.objects.filter(email=email).exists():
    #         messages.error(request, f'Email Already exist')
    #     return email

    # def clean_password2(self, request, format=None):
    #     password1 = self.cleaned_data.get('password1')
    #     password2 = self.cleaned_data.get('password2')
    #     if password1 and password2 and password1 != password2:
    #         messages.info(request, f'Password does not match')
    #         raise forms.ValidationError(
    #             'Password mismatch',
    #         )
    #     return password2
