from django.shortcuts import render, redirect
from .forms import UserRegisterForm
from django.contrib import messages


# sign up user view function
def su(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(
                request, f'[{username}] You are registerd now!')
            return redirect('signin')
    else:
        form = UserRegisterForm()
    return render(request, 'susi/signup.html', {'form': form})
