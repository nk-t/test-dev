from django.urls import path
from susi import views
from django.contrib.auth import views as auth_view


urlpatterns = [
    path('su/', views.su, name='signup'),
    path('si/', auth_view.LoginView.as_view(
        template_name='susi/signin.html'),
        name='signin'),
    path('logout/', auth_view.LogoutView.as_view(
        template_name='susi/logout.html'),
        name='logout'),
]
