from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path('', include('home.urls')),
    path('admin/', admin.site.urls),
    path('susi/', include('susi.urls')),
    path('qqqa/', include('qqqa.urls')),
    path('', include('accounts.urls'))
]
