from django.db import models
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.models import User
# from django.db.models.signals import pre_save
# from django.dispatch import receiver
# from django.utils.text import slugify

from ckeditor.fields import RichTextField


class QqqaPost(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    qq_title = models.CharField(max_length=200)
    slug = models.SlugField(
        max_length=100, unique=True)
    qq_details = RichTextField(blank=True, null=True)
    likes = models.ManyToManyField(User, related_name="q_like", blank=True)
    posted_date = models.DateTimeField(default=timezone.now)
    disable_comments = models.BooleanField(blank=True, null=True)

    def __str__(self):
        return self.qq_title

    def get_absolute_url(self):
        kwargs = {
            'id': self.id,
            'slug': self.slug
        }
        return reverse("question-detail-view", kwargs=kwargs)

    def get_vote_url(self):
        kwargs = {
            'id': self.id,
            'slug': self.slug
        }
        return reverse("question-vote", kwargs=kwargs)

    def get_nonvote_url(self):
        kwargs = {
            'id': self.id,
            'slug': self.slug
        }
        return reverse("question-nonvote", kwargs=kwargs)

    # def save(self, *args, **kwargs):
    #     self.slug = slugify(self.qq_title)
    #     super(QqqaPost, self).save(*args, **kwargs)


# @receiver(pre_save, sender=QqqaPost)
# def pre_save_slug_receiver(sender, **kwargs):
#     slug = slugify(kwargs['instance'].qq_title)
#     kwargs['instance'].slug = slug


class QqqaPostComment(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(
        QqqaPost, on_delete=models.CASCADE, related_name='comments')
    comment = models.CharField(max_length=200)
    c_posted_time = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.comment

    def get_absolute_url(self):
        kwargs = {
            'id': self.id,
            'slug': self.slug,
        }
        return reverse("question-detail-view", kwargs=kwargs)
