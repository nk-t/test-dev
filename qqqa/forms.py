from django import forms
from .models import QqqaPostComment


class QqqaCommentsForm(forms.ModelForm):
    class Meta:
        model = QqqaPostComment
        fields = [
            'comment'
        ]
