from django.contrib import admin
from .models import QqqaPost, QqqaPostComment

admin.site.register(QqqaPost)
admin.site.register(QqqaPostComment)
