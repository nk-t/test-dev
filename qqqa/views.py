from django.shortcuts import get_object_or_404, render, HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
# from django.contrib.auth.decorators import login_required
from django.views.generic import (ListView,
                                  CreateView,
                                  UpdateView, DeleteView, RedirectView
                                  )
from django.urls import reverse_lazy

# from django.http import JsonResponse
# from django.template.loader import render_to_string

from .models import QqqaPost, QqqaPostComment
from .forms import QqqaCommentsForm


# def question(request):    # function based views
#     context = {
#         'qqqa_posts': QqqaPost.objects.all()
#     }
#     return render(request, 'qqqa/allquestion.html', context)


class QquestionListView(ListView):    # class based views - ListView
    model = QqqaPost
    template_name = 'qqqa/allquestion.html'
    # template_name = 'question'
    context_object_name = 'qqqa_posts'
    ordering = ['-posted_date']
    pk_url_kwarg = "pk"
    slug_url_kwarg = "slug"
    query_pk_and_slug = True


# class based views - DetailView
# class QuestionDetailView(DetailView):
#     model = QqqaPost
#     template_name = 'qqqa/qqqapost_detail.html'
#     pk_url_kwarg = "pk"
#     slug_url_kwarg = "slug"
#     query_pk_and_slug = True

    # comments = QqqaPostComment.objects.all()
    # context_object_name = 'qqqa_postscomment'

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context['comments'] = QqqaPostComment.objects.all()
    #     return context


# function based view - for individual qustion details
def post_details_view(request, id, slug):
    post = get_object_or_404(QqqaPost, id=id, slug=slug)
    comments = QqqaPostComment.objects.filter(post=post).order_by('-id')

    # save comments by user
    if request.method == "POST":
        comment_form = QqqaCommentsForm(request.POST or None)
        if comment_form.is_valid():
            comment = request.POST.get('comment')
            comment = QqqaPostComment.objects.create(
                post=post, author=request.user, comment=comment)
            comment.save()
            return HttpResponseRedirect(post.get_absolute_url())
    else:
        comment_form = QqqaCommentsForm()

    context = {
        'post': post,
        'comments': comments,
        'comment_form': comment_form
    }
    return render(request, 'qqqa/qqqapost_detail.html', context)


# class CommentDetailView(DetailView):   # This is not working
#     model = QqqaPostComment
#     template_name = 'qqqa/qqqapost_detail.html'
#     context_object_name = 'qqqa_postscomment'
#     comments = QqqaPostComment.objects.all()
#     pk_url_kwarg = "pk"
#     slug_url_kwarg = "slug"
#     query_pk_and_slug = True


# class based view - CreateView
class QuestionCreateView(LoginRequiredMixin, CreateView):
    model = QqqaPost
    fields = ['qq_title', 'qq_details', 'slug', 'disable_comments']

    # taking author instance id with post_id
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


# class based view - Updateview
class QuestionUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = QqqaPost
    fields = ['qq_title', 'qq_details', 'slug', 'disable_comments']
    pk_url_kwarg = "pk"
    slug_url_kwarg = "slug"
    query_pk_and_slug = True

    # taking author instance id with post_Id
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    # checking + matching the author id with post_id
    def test_func(self):
        qqqa_post = self.get_object()
        if self.request.user == qqqa_post.author:
            return True
        return False


# class based view - DeleteView
class QuestionDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = QqqaPost
    success_url = reverse_lazy('question')   # redirect after success deletion

    # checking + matching the author id with post_id
    def test_func(self):
        qqqa_post = self.get_object()
        if self.request.user == qqqa_post.author:
            return True
        return False


class QuestionLike(LoginRequiredMixin, RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        slug = self.kwargs.get("slug")
        obj = get_object_or_404(QqqaPost, slug=slug)
        url_ = obj.get_absolute_url()
        user = self.request.user
        if user.is_authenticated:
            if user in obj.likes.all():
                obj.likes.remove(user)
            else:
                obj.likes.add(user)
        return url_

        # like by Ajax request - yet not done or not working and  not finished
        # def ajax_request(request):
        #     if request.is_ajax():
        #         html = render_to_string('qqqa/qqqapost_like_section.html')
        #         return JsonResponse({'form': html})
