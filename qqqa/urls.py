from django.urls import path
from .views import (QquestionListView,
                    QuestionCreateView,
                    QuestionUpdateView, QuestionDeleteView, QuestionLike
                    )
from qqqa import views


urlpatterns = [
    # path('', views.question, name='question'),
    path('', QquestionListView.as_view(), name='question'),


    #     path('q/<int:pk>/', QuestionDetailView.as_view(),
    #          name='question-detail-view'),
    #     path('q/<pk>/<slug>/', QuestionDetailView.as_view(),
    #          name='question-detail-view'),


    path('q/<id>/<slug>/', views.post_details_view,
         name='question-detail-view'),
    path('q/new/', QuestionCreateView.as_view(),
         name='question-create-view'),



    path('q/<pk>/<slug>/update/', QuestionUpdateView.as_view(),
         name='question-update-view'),
    path('q/<pk>/<slug>/delete/', QuestionDeleteView.as_view(),
         name='question-delete-view'),


    #     path('q/<pk>/<slug>/vote/', QuestionLike.as_view(),
    #          name='question-vote'),
    #     path('q/<pk>/<slug>/nonvote/', QuestionLike.as_view(),
    #          name='question-nonvote'),



    path('q/<id>/<slug>/vote/', QuestionLike.as_view(),
         name='question-vote'),
    path('q/<id>/<slug>/nonvote/', QuestionLike.as_view(),
         name='question-nonvote'),
]
